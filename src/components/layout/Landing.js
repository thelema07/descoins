import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./layout.css";

class Landing extends Component {
  render() {
    return (
      <div className="landing bg-dark">
        <div className="dark-overlay landing-inner text-light">
          <div className="container">
            <div className="row">
              <div className="col-md-12 mt-4 text-center landing-box">
                <h1 className="display-3 mb-4">
                  <i className="fab fa-gg-circle mr-3" />
                  Global Currency
                </h1>
                <p className="lead">
                  <strong>
                    Get Online information about currencies and markets around
                    the world
                  </strong>
                </p>
                <Link
                  to="/register"
                  className="shadow-sm btn btn-warning mr-2 btn-sm"
                >
                  Sign Up
                </Link>
                <Link to="/login" className="shadow-sm btn btn-light btn-sm">
                  Login
                </Link>
              </div>
              <div className="jumbotron mt-5 mb-5 pb-4 bg-warning ">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-4 col-xs-12 mt-3">
                      <div className="card bg-dark">
                        <div className="card-body">
                          <h5 className="card-title">
                            Special title treatment
                          </h5>
                          <p className="card-text">
                            With supporting text below as a natural lead-in to
                            additional content.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4 col-xs-12 mt-3">
                      <div className="card bg-dark">
                        <div className="card-body">
                          <h5 className="card-title">
                            Special title treatment
                          </h5>
                          <p className="card-text">
                            With supporting text below as a natural lead-in to
                            additional content.
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4 col-xs-12 mt-3">
                      <div className="card bg-dark">
                        <div className="card-body">
                          <h5 className="card-title">
                            Special title treatment
                          </h5>
                          <p className="card-text">
                            With supporting text below as a natural lead-in to
                            additional content.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
