import React from "react";

export default function Footer() {
  return (
    <footer className="footer-currency bg-warning text-dark p-5 text-center">
      Copyright &copy; {new Date().getFullYear()} TH7 Design Project
    </footer>
  );
}
