import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";
import TextFieldGroup from "../common/TextFieldGroup";
import Spinner from "../common/Spinner";

import "./auth.css";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
      loading: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.timerToast = this.timerToast.bind(this);
  }

  timerToast(parent, child, time) {
    setTimeout(() => {
      let body = document.getElementById(parent);
      let toast = document.querySelector(child);
      body.removeChild(toast);
    }, time);
  }

  onSubmit(e) {
    this.setState({ loading: true });
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(userData);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    this.timerToast("login", "#toast-login", 1900);

    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
      this.setState({
        loading: false
      });
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
        loading: false
      });
    }
  }

  render() {
    let toastContent;

    const fullBackground = {
      height: "100vh"
    };
    const { errors } = this.state;

    let loginBoxContent = (
      <div className="shadow card bg-warning m-auto col-md-8 col-sm-12">
        <div className="card-body">
          <form onSubmit={this.onSubmit} noValidate>
            <TextFieldGroup
              placeholder="Email Address"
              name="email"
              type="email"
              value={this.state.email}
              onChange={this.onChange}
              error={errors.email}
            />
            <TextFieldGroup
              placeholder="Password"
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.onChange}
              error={errors.password}
            />
            <input
              type="submit"
              className="shadow-sm btn btn-dark btn-sm btn-block mt-4"
            />
          </form>
        </div>
      </div>
    );

    return (
      <div id="login" className="login bg-dark" style={fullBackground}>
        <div
          id="toast-login"
          class="shadow-sm bg-warning p-3 rounded toast-slide-down"
        >
          <strong class="text-dark">Welcome! please login</strong>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center text-warning mt-5">
                <i className="fas fa-sign-in-alt text-warning mr-3" />
                Log In
              </h1>
              <p className="lead text-center text-warning">
                Sign in to your GlobalCurrencies Account
              </p>
              {!this.state.loading ? (
                loginBoxContent
              ) : (
                <div className="d-flex justify-content-center">
                  <Spinner />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
