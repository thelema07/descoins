import React from "react";
import { Link } from "react-router-dom";

const ProfileButtons = () => {
  return (
    <div className="btn-group btn-group-toggle mt-4">
      <Link to="/edit-profile" className="btn btn-warning">
        <i className="fas fa-edit text-dark mr-1" /> Edit Profile
      </Link>
      <Link to="/currency-converter" className="btn btn-warning">
        <i className="fas fa-dollar-sign text-dark mr-1" /> Currency Conversion
      </Link>
      <Link to="/my-stats" className="btn btn-warning">
        <i className="fas fa-chart-bar text-dark mr-1" /> My Stats
      </Link>
    </div>
  );
};

export default ProfileButtons;
