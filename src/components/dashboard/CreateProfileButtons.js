import React from "react";
import { Link } from "react-router-dom";

const CreateProfileButtons = () => {
  return (
    <div className="btn-group btn-group-toggle mt-4">
      <Link to="/create-profile" className="btn btn-outline-warning">
        <i className="fas fa-user-circle text-warning mr-1" />
        Create Profile
      </Link>
    </div>
  );
};

export default CreateProfileButtons;
