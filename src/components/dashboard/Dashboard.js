import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { getCurrentProfile } from "../../actions/profileActions";

import ProfileButtons from "./ProfileButtons";
import CreateProfileButtons from "./CreateProfileButtons";

import isEmpty from "../../validation/is-empty";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      handle: "",
      name: "",
      birthday: "",
      country: "",
      location: "",
      gender: "",
      description: "",
      twitter: "",
      facebook: "",
      youtube: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      profile.handle = !isEmpty(profile.handle) ? profile.handle : "";
      profile.name = !isEmpty(profile.name) ? profile.name : "";
      profile.birthday = !isEmpty(profile.birthday) ? profile.birthday : "";
      profile.country = !isEmpty(profile.country) ? profile.country : "";
      profile.location = !isEmpty(profile.location) ? profile.location : "";
      profile.gender = !isEmpty(profile.gender) ? profile.gender : "";
      profile.description = !isEmpty(profile.description)
        ? profile.description
        : "";
      profile.twitter = !isEmpty(profile.twitter) ? profile.twitter : "";
      profile.facebook = !isEmpty(profile.facebook) ? profile.facebook : "";
      profile.youtube = !isEmpty(profile.youtube) ? profile.youtube : "";

      this.setState({
        handle: profile.handle,
        name: profile.name,
        birthday: profile.birthday,
        country: profile.country,
        location: profile.location,
        gender: profile.gender,
        description: profile.description,
        twitter: profile.twitter,
        facebook: profile.facebook,
        youtube: profile.youtube
      });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;
    let dashboardContent;
    if (profile == null || loading) {
    } else {
      if (profile.handle === "") {
        dashboardContent = (
          <div>
            <CreateProfileButtons />
          </div>
        );
      } else {
        dashboardContent = (
          <div>
            <ProfileButtons />
          </div>
        );
      }
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <div className="btn-group btn-group-toggle mt-4">
              {dashboardContent}
            </div>
            <div className="jumbotron mt-4 border border-warning bg-dark">
              <h1 className="display-4 text-warning">Hello, {user.name}</h1>
              <p className="lead text-warning">
                Track currencies, check currency history, search and manage
                currency value according to your current country, and use the
                currency conversion feature.
              </p>
              <p className="text-warning">
                Use all the amazing features and take advantage of our live
                information service.
              </p>
            </div>

            <div className="accordion pb-4" id="accordionExample">
              <div className="card bg-warning text-dark">
                <div className="card-header" id="headingOne">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-dark text-warning"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Basic Information
                      <i className="fas fa-info-circle text-warning mr-1 ml-2" />
                    </button>
                  </h2>
                </div>

                <div
                  id="collapseOne"
                  className="collapse show"
                  aria-labelledby="headingOne"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    <ul className="list-group list-group-flush ">
                      <li className="list-group-item bg-transparent">
                        <strong>Name: </strong> {this.state.name}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Nickname: </strong> {this.state.handle}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Country: </strong> {this.state.country}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Location: </strong> {this.state.location}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Gender: </strong>{" "}
                        {this.state.gender !== ""
                          ? this.state.gender
                          : " - - -"}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="card bg-warning text-dark">
                <div className="card-header" id="headingTwo">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-dark text-warning"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Description
                      <i className="fas fa-window-restore text-warning mr-1 ml-2" />
                    </button>
                  </h2>
                </div>
                <div
                  id="collapseTwo"
                  className="collapse"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    <ul className="list-group list-group-flush ">
                      <li className="list-group-item bg-transparent">
                        {this.state.description}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="card bg-warning text-dark">
                <div className="card-header" id="headingThree">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-dark text-warning"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseThree"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      Social Media
                      <i className="fas fa-share-square text-warning mr-1 ml-2" />
                    </button>
                  </h2>
                </div>
                <div
                  id="collapseThree"
                  className="collapse"
                  aria-labelledby="headingThree"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    <ul className="list-group list-group-flush ">
                      <li className="list-group-item bg-transparent">
                        <strong>Twitter: </strong> {this.state.twitter}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Facebook: </strong> {this.state.facebook}
                      </li>
                      <li className="list-group-item bg-transparent">
                        <strong>Youtube </strong> {this.state.youtube}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile }
)(Dashboard);
