import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import InputGroup from "../common/InputGroup";
import { createProfile, getCurrentProfile } from "../../actions/profileActions";
import isEmpty from "../../validation/is-empty";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      handle: "",
      name: "",
      birthday: "",
      country: "",
      location: "",
      gender: "",
      description: "",
      twitter: "",
      facebook: "",
      youtube: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;

      profile.handle = !isEmpty(profile.handle) ? profile.handle : "";
      profile.name = !isEmpty(profile.name) ? profile.name : "";
      profile.birthday = !isEmpty(profile.birthday) ? profile.birthday : "";
      profile.country = !isEmpty(profile.country) ? profile.country : "";
      profile.location = !isEmpty(profile.location) ? profile.location : "";
      profile.gender = !isEmpty(profile.gender) ? profile.gender : "";
      profile.description = !isEmpty(profile.description)
        ? profile.description
        : "";
      profile.twitter = !isEmpty(profile.twitter) ? profile.twitter : "";
      profile.facebook = !isEmpty(profile.facebook) ? profile.facebook : "";
      profile.youtube = !isEmpty(profile.youtube) ? profile.youtube : "";

      this.setState({
        handle: profile.handle,
        name: profile.name,
        birthday: profile.birthday,
        country: profile.country,
        location: profile.location,
        gender: profile.gender,
        description: profile.description,
        twitter: profile.twitter,
        facebook: profile.facebook,
        youtube: profile.youtube
      });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const profileData = {
      handle: this.state.handle,
      name: this.state.name,
      country: this.state.country,
      location: this.state.location,
      gender: this.state.gender,
      description: this.state.description,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      youtube: this.state.youtube
    };

    this.props.createProfile(profileData, this.props.history);
  }

  render() {
    const { errors, displaySocialInputs } = this.state;

    let socialInputs;

    if (displaySocialInputs) {
      socialInputs = (
        <div>
          <InputGroup
            placeholder="Twitter Profile URL"
            name="twitter"
            icon="fab fa-twitter"
            value={this.state.twitter}
            onChange={this.onChange}
            error={errors.twitter}
          />
          <InputGroup
            placeholder="Facebook Profile URL"
            name="facebook"
            icon="fab fa-facebook"
            value={this.state.facebook}
            onChange={this.onChange}
            error={errors.facebook}
          />
          <InputGroup
            placeholder="Youtube Profile URL"
            name="youtube"
            icon="fab fa-youtube"
            value={this.state.youtube}
            onChange={this.onChange}
            error={errors.youtube}
          />
        </div>
      );
    }

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center mt-4 text-warning">
                Edit Your Profile
              </h1>
              <p className="lead text-center text-warning">
                Go ahead and update your information whenever you want
              </p>
              <small className="d-block pb-3 text-warning">
                * = required fields
              </small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Profile Handle"
                  name="handle"
                  value={this.state.handle}
                  onChange={this.onChange}
                  error={errors.handle}
                  info="A unique handle for your profile URL. Your full name, company name, nickname"
                />
                <TextFieldGroup
                  placeholder="* Name"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  error={errors.name}
                />
                {/* <TextFieldGroup
                  placeholder="Birthday"
                  name="birthday"
                  type="date"
                  value={this.state.birthday}
                  onChange={this.onChange}
                  error={errors.birthday}
                /> */}
                <TextFieldGroup
                  placeholder="Country"
                  name="country"
                  value={this.state.country}
                  onChange={this.onChange}
                  error={errors.country}
                />
                <TextFieldGroup
                  placeholder="Location"
                  name="location"
                  value={this.state.location}
                  onChange={this.onChange}
                  error={errors.location}
                />
                <div className="form-check-inline">
                  <label className="form-check-label text-warning">
                    <input
                      type="radio"
                      className="form-check-input"
                      name="Gender"
                      value="male"
                    />
                    Male
                  </label>
                </div>
                <div className="form-check-inline">
                  <label className="form-check-label text-warning">
                    <input
                      type="radio"
                      className="form-check-input"
                      name="Gender"
                      value="female"
                    />
                    Female
                  </label>
                </div>
                <TextAreaFieldGroup
                  placeholder="Description"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="Tell us a little about yourself"
                />

                <div className="mb-3">
                  <button
                    type="button"
                    onClick={() => {
                      this.setState(prevState => ({
                        displaySocialInputs: !prevState.displaySocialInputs
                      }));
                    }}
                    className="btn btn-warning btn-block"
                  >
                    Add Social Media
                  </button>
                  <span className="text-muted">Optional</span>
                </div>
                {socialInputs}
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-warning btn-block mt-4 mb-5"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

EditProfile.propTypes = {
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { createProfile, getCurrentProfile }
)(withRouter(EditProfile));
