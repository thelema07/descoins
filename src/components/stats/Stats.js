import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { getCurrentProfile } from "../../actions/profileActions";

class Stats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      conversionData: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    if (nextProps.profile.profile) {
      const profile = nextProps.profile.profile;
      this.setState({
        conversionData: profile.conversion
      });
    }
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  render() {
    const conversion = this.state.conversionData;

    let conversionData = {};
    let totalMoneyRequest = 0;

    let lookCurrency = {};

    conversion.forEach(request => {
      totalMoneyRequest += Number(request.origin);
      if (lookCurrency[request.from] > 0) {
        lookCurrency[request.from]++;
      } else {
        lookCurrency[request.from] = 1;
      }
    });

    const currencyLookupResult = Object.entries(lookCurrency);
    const mostRequestedCurrency = currencyLookupResult.sort(
      (a, b) => b[1] - a[1]
    );

    const listMoneyRequest = mostRequestedCurrency.map(req => (
      <li key={req[0]}>
        {req[0]} ( {req[1]} )
      </li>
    ));

    conversionData.numRequest = conversion.length;
    conversionData.totalMoney = totalMoneyRequest;

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <div className="jumbotron mt-4 border border-warning bg-dark">
              <h1 className="display-4 text-warning">My Currency Status</h1>
              <p className="lead text-warning">
                Your amounts consulted, how many times you did it, all your
                currency in one place. Use all the amazing features and take
                advantage of our live information service.
              </p>
            </div>

            <div class="card bg-warning mb-3">
              <div class="card-header">
                <ul
                  class="nav nav-tabs card-header-tabs pull-right"
                  id="myTab"
                  role="tablist"
                >
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="home-tab"
                      data-toggle="tab"
                      href="#home"
                      role="tab"
                      aria-controls="home"
                      aria-selected="true"
                    >
                      Total Request
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="profile-tab"
                      data-toggle="tab"
                      href="#profile"
                      role="tab"
                      aria-controls="profile"
                      aria-selected="false"
                    >
                      Mused Currencies
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="contact-tab"
                      data-toggle="tab"
                      href="#contact"
                      role="tab"
                      aria-controls="contact"
                      aria-selected="false"
                    >
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="myTabContent">
                  <div
                    class="tab-panel fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <h5 className="text-dark">
                      My Requests:
                      {listMoneyRequest}
                    </h5>
                  </div>
                  <div
                    class="tab-panel fade "
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <h5 className="text-dark">
                      Total Requests:
                      {conversionData.numRequest}
                    </h5>
                  </div>
                  <div
                    class="tab-panel fade"
                    id="contact"
                    role="tabpanel"
                    aria-labelledby="contact-tab"
                  >
                    ...
                  </div>
                </div>
              </div>
            </div>

            <div class="card bg-warning mb-3">
              <div class="card-header">
                <ul
                  class="nav nav-tabs card-header-tabs pull-right"
                  id="myTab"
                  role="tablist"
                >
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="home-tab"
                      data-toggle="tab"
                      href="#home"
                      role="tab"
                      aria-controls="home"
                      aria-selected="true"
                    >
                      Total Request
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="profile-tab"
                      data-toggle="tab"
                      href="#profile"
                      role="tab"
                      aria-controls="profile"
                      aria-selected="false"
                    >
                      Mused Currencies
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      class="nav-link bg-dark border border-warning text-warning"
                      id="contact-tab"
                      data-toggle="tab"
                      href="#contact"
                      role="tab"
                      aria-controls="contact"
                      aria-selected="false"
                    >
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="myTabContent">
                  <div
                    class="tab-panel fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <h5 className="text-dark">
                      My Requests:
                      {listMoneyRequest}
                    </h5>
                  </div>
                  <div
                    class="tab-panel fade "
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <h5 className="text-dark">
                      Total Requests:
                      {conversionData.numRequest}
                    </h5>
                  </div>
                  <div
                    class="tab-panel fade"
                    id="contact"
                    role="tabpanel"
                    aria-labelledby="contact-tab"
                  >
                    ...
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Stats.propTypes = {
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile }
)(Stats);
