import React from "react";

export default () => {
  return (
    <div>
      <div
        className="spinner-grow m-5 text-warning"
        style={{ width: "5rem", height: "5rem" }}
        role="status"
      >
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  );
};
