import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import SelectListGroup from "../common/SelectListGroup";
import TextFieldGroup from "../common/TextFieldGroup";

import { currencyConversion } from "../../actions/currencyActions";

class CurrencyConverter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currency1: "",
      value1: "",
      currency2: "",
      conversion: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.convertCurrency = this.convertCurrency.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }

    console.log(nextProps);
    if (nextProps.currency) {
      const conversion = nextProps.currency.conversion;
      console.log(conversion);

      this.setState({
        currency1: "",
        value1: "",
        currency2: "",
        conversion: conversion.text
      });
    }
  }

  convertCurrency() {
    const conversionData = {
      currency1: this.state.currency1,
      value1: this.state.value1,
      currency2: this.state.currency2
    };

    this.props.currencyConversion(conversionData);
  }

  render() {
    const options = [
      { label: "* Select Currency", value: 0 },
      { label: "US Dollar (USA)", value: "USD" },
      { label: "Euro (Europe)", value: "EUR" },
      { label: "Sterling Pound (UK)", value: "GBP" }
    ];

    return (
      <div className="create-profile pb-4 pt-4">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center mt-4 text-warning">
                <i class="fas fa-exchange-alt text-warning mr-3" />
                Currency Converter
              </h1>
              <p className="lead text-center text-warning">
                Convert Rates for the most popular currencies in the world
              </p>
            </div>
          </div>
          <div className="row d-flex justify-content-center">
            <div className="col-md-4">
              <div className="shadow-sm card text-dark bg-warning mb-4 p-2">
                <div className="card-body">
                  <h3 className="card-title text-dark">
                    Your Funds
                    <i class="fas fa-hand-holding-usd text-dark ml-3" />
                  </h3>
                  <TextFieldGroup
                    placeholder="From"
                    type="number"
                    name="value1"
                    value={this.state.value1}
                    onChange={this.onChange}
                    options={options}
                    info="Initial currency"
                  />
                  <h6 className="card-subtitle mb-2 text-dark">
                    <i className="fas fa-arrow-right mr-3" />
                    From
                  </h6>
                  <SelectListGroup
                    placeholder="From: "
                    name="currency1"
                    value={this.state.currency1}
                    onChange={this.onChange}
                    options={options}
                    info="Initial currency"
                  />
                  <h6 className="card-subtitle mb-2 text-dark">
                    <i class="fas fa-arrow-right mr-3" />
                    To
                  </h6>
                  <SelectListGroup
                    placeholder="To: "
                    name="currency2"
                    value={this.state.currency2}
                    onChange={this.onChange}
                    options={options}
                    info="Destination currency"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <h3 className="text-center pt-1 pb-1 text-warning">
                Conversion
                <i class="fas fa-sync ml-3" />
              </h3>
              <div className="shadow-sm card text-dark bg-warning mb-3 pb-1">
                <input
                  className="btn btn-dark m-4"
                  type="submit"
                  value="Check"
                  onClick={this.convertCurrency}
                />
              </div>
              <div className="shadow-sm card text-dark bg-warning mb-5">
                <h3 className="text-center pt-4">Conversion Result</h3>
                <p className="text-center p-4">{this.state.conversion}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CurrencyConverter.propTypes = {
  currencyConversion: PropTypes.func.isRequired,
  currency: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  currency: state.currency,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { currencyConversion }
)(CurrencyConverter);
