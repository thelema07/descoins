import axios from "axios";

import {
  CLEAR_CURRENT_PROFILE,
  PROFILE_LOADING,
  GET_ERRORS,
  GET_PROFILE
} from "./types";

export const createProfile = (profileData, history) => dispatch => {
  axios
    .post("https://descoins-backend.herokuapp.com/api/profiles", profileData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading);
  axios
    .get("https://descoins-backend.herokuapp.com/api/profiles")
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PROFILE,
        payload: {}
      })
    );
};

export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING
  };
};

export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  };
};
