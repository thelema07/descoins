// API key ER4vUGjQSkj05T6HhwAzmCVTUDHORODX
// https://forex.1forge.com/1.0.3/quotes?pairs=EURUSD,GBPJPY,AUDUSD&api_key=ER4vUGjQSkj05T6HhwAzmCVTUDHORODX

//https://forex.1forge.com/1.0.3/convert?from=USD&to=EUR&quantity=310000&api_key=ER4vUGjQSkj05T6HhwAzmCVTUDHORODX

import axios from "axios";

import { GET_ERRORS, GET_CONVERSION, CONVERSION_LOADING } from "./types";

export const currencyConversion = data => dispatch => {
  axios
    .get(
      `https://descoins-backend.herokuapp.com/api/currency/convert/${
        data.currency1
      }/${data.currency2}/${data.value1}`
    )
    .then(res =>
      dispatch({
        type: GET_CONVERSION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {}
      })
    );
};

export const setCurrencyLoading = () => {
  return {
    type: CONVERSION_LOADING
  };
};
