import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import profileReducer from "./profileReducer";
import currencyReducer from "./currencyReducer";

export default combineReducers({
  auth: authReducer,
  profile: profileReducer,
  currency: currencyReducer,
  errors: errorReducer
});
