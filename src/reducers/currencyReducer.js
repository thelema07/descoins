import { GET_CONVERSION, CONVERSION_LOADING } from "../actions/types";

const initialState = {
  
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CONVERSION_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_CONVERSION:
      return {
        ...state,
        conversion: action.payload,
        loading: false
      };
    default:
      return state;
  }
}
